# 1.0.0 (2019-11-20)


### Features

* Adding Dockerfile and CI pipeline ([ccc484e](https://gitlab.com/dreamer-labs/repoman/dl-kcov-test/commit/ccc484e))
